##########################################################################
# Mass Spec Prediction and Identification of Metabolites
#
# cfm-test/CMakeLists.txt
#
# Author: Felicity Allen
# Created: August 2013
##########################################################################
 
set ( SRC_FILES  test.cpp 
                 tests/comms_test.cpp tests/comms_test.h 
                 tests/em_test.cpp tests/em_test.h
		tests/fraggen_test.cpp tests/fraggen_test.h
		)
                                           
add_executable ( cfm-test ${SRC_FILES} )
target_link_libraries ( cfm-test cfm-code ${REQUIRED_LIBS} )

install ( TARGETS cfm-test 
          DESTINATION ${CFM_OUTPUT_DIR} )
          
install ( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/tests/test_data
          DESTINATION ${CFM_OUTPUT_DIR}/tests )          
          
          
          